
Using an [`InceptionV3` model](https://keras.io/applications/#inceptionv3) to classify images with class 
imbalance using [focal loss](https://arxiv.org/abs/1708.02002) and 
[`class_weight`](https://keras.io/models/model/#methods). Data augmentation applying only 90 degree 
rotations. Note: not yet tested. 
